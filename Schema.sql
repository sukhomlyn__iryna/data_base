
CREATE TYPE type_pledge AS ENUM ('flat','car','furniture','jewels','land');
CREATE TABLE pledge(
	id_pledge			serial PRIMARY KEY,
	name				varchar(50) NOT NULL,
	price				integer, CHECK(price > 0),
	number                          integer CHECK(number < 10)
);


CREATE TABLE manager(
	id_manager			serial PRIMARY KEY,
	name				varchar(50)  NOT NULL,
     	second_name			varchar(50)  NOT NULL,
	number_client		        integer,CHECK(number_client > 0),
	job_hours             		integer,CHECK(job_hours > 4),
    	 month_report          		integer, CHECK(month_report > 2),
    	 year_report          		integer,CHECK(year_report > 1) 
);

CREATE TABLE partner(
	id_partner			serial PRIMARY KEY,
	name				varchar(50) NOT NULL,
     	second_name			varchar(50)NOT NULL,
     	spended_money         		numeric(18,2),
     	profit                		numeric(18,2),
	balance                         numeric(18,2),
	job				varchar(50) NOT NULL
);


CREATE TYPE type_partner AS ENUM ('business','employee','borrower');
CREATE TABLE relation_partner(
	id_type_relation		serial PRIMARY KEY,
	type				type_partner NOT NULL,
	period				int CHECK(period > 0)	
);

CREATE TYPE type_responsible_person AS ENUM ('chief_specialist','chief_lawyer','derektor_sector','head_department');
CREATE TABLE deal_types(
	id_type				serial PRIMARY KEY,
        name				varchar(50) NOT NULL,
     	responsible_person		type_responsible_person	NOT NULL
);

CREATE TABLE currancy(
	id_currancy			serial PRIMARY KEY,
    	name				varchar(50)  NOT NULL UNIQUE,
     	exchange_rate         		float CHECK( exchange_rate >0),
     	current_dat		        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP 
);


CREATE TABLE deal(
	id_deal			serial PRIMARY KEY,
     	id_type			integer REFERENCES deal_types 
                                (id_type) ON DELETE CASCADE,
	id_currancy		integer REFERENCES currancy 
                                (id_currancy) ON DELETE CASCADE,

	current_dat             TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,	
	period                  integer CHECK(period>0)
);


CREATE TYPE type_region AS ENUM ('Kyiv','Kharkiv','Donetsk','Simferopol','Lviv','Lugansk','Ternopyl','Vinnitsia','Chernivtci','Lutsk');
CREATE TABLE client(
	id_client		serial PRIMARY KEY,
	id_manager		integer REFERENCES manager 
                          (id_manager) ON DELETE CASCADE,
 	id_pledge		integer REFERENCES pledge 
                          (id_pledge) ON DELETE CASCADE,
	id_deal			integer REFERENCES deal 
                          (id_deal) ON DELETE CASCADE,
	name			varchar(50) NOT NULL,
	second_name		varchar(50) NOT NULL,
        telephone               numeric(18) ,
	region			type_region,
	job			varchar(50) NOT NULL
);

CREATE TABLE client_partner(
	id_client			integer REFERENCES client (id_client) ON DELETE CASCADE,
	id_partner			integer REFERENCES partner (id_partner) ON DELETE CASCADE,
        id_type_relation		integer REFERENCES relation_partner(id_type_relation) ON DELETE CASCADE,     
	PRIMARY KEY (id_client, id_partner,id_type_relation)
);


CREATE TABLE accounts(
	id_account		serial PRIMARY KEY,
	id_client		int REFERENCES client 
                                (id_client) ON DELETE CASCADE,
	number_acc              integer,
	balance			float NOT NULL DEFAULT 0 ,
	costs			float NOT NULL DEFAULT 0 ,
	profit			float NOT NULL DEFAULT 0 
);


