

INSERT INTO pledge(name,price,number) VALUES
('flat','20000','1'),('car','10300','2'),('car','30000','1');

INSERT INTO manager(name,second_name,number_client,job_hours,month_report,year_report) VALUES
('Ivan','Pavlenko','28','8','10','3'),('Ivan','kravchenko','20','6','5','2'),('Olena','Rakhmanova','30','9','12','5');

INSERT INTO partner(name,second_name,spended_money,profit,job) VALUES
('Andriy','Savchenko','13000','20000','loyer'),('Polina','Sergienko','10000','300000','manager'),('Irina','Tomenko','20000','40000','programmer');

INSERT INTO relation_partner(type,period) VALUES
('business','2'),('employee','2'),('borrower','1');

   
INSERT INTO relation_partner(type, period) VALUES 
('business', '2'),('business','1'), ('employee','5');

INSERT INTO deal_types( name, responsible_person)  VALUES 
('deposit','chief_specialist' ),('deposit','chief_specialist'),('credit','chief_lawyer');

INSERT INTO currancy(name,exchange_rate,current_dat) VALUES
('dollar','2.20','20.03.2012'),('euro','11.00','20.03.2012'),('ruble','0.20','22.03.2012');

INSERT INTO deal(id_type,id_currancy,current_dat,period) VALUES
('1','1','21.03.2012','2'),('2','2','21.03.2012','4'),('3','3','22.03.2012','5');

INSERT INTO client(id_manager,id_pledge,id_deal,name,second_name,telephone ,region,job) VALUES
('1','4','2','Alena','Stepanenko','0980988098','Kyiv','student'),
('1','3','3','Sasha','Zastupaylo','0980538098','Kyiv','student'),
('2','5','3','Dima','Babienko','0979538098','Kyiv','student');

INSERT INTO client_partner(id_client,id_partner,id_type_relation) VALUES
('4','1','1'),('4','3','1'),('5','2','3');

INSERT INTO accounts(id_client,number_acc,balance,costs,profit) VALUES
('4','3','12319.5','1487','45987.8'),('5','1','11319.5','2447','43087.8'),('6','3','52319.5','2487','48987.8');
