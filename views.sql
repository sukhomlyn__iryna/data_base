--�������� �������������(CREATE VIEW)

 create VIEW clinet_balance
	AS SELECT c.id_client,c.name, c.second_name, c.telephone, c.region, c.job, 
		a.id_account,a.number_acc,a.balance,a.costs,a.profit	
 	FROM client c,accounts a
	 	WHERE a.id_client = c.id_client
		and a.balance >=2000 ;

--�������� INSERT/UPDATE/DELETE(CREATE RULE):

--INSERT
CREATE OR REPLACE RULE client_balance_in AS ON INSERT TO client_balance_vw
        DO INSTEAD (
        INSERT INTO client (name, second_name,telephone, region, job)
        VALUES (NEW.name, NEW.second_name, NEW.telephone, NEW. region, NEW.job);  
	INSERT INTO accounts (number_acc, balance,costs, profit)
        VALUES (NEW.number_acc, NEW.balance,NEW.costs, NEW.profit);    
        );

--UPDATE 
CREATE OR REPLACE RULE client_balance_up AS ON UPDATE TO client_balance_vw 
        DO INSTEAD (
        UPDATE Client
        SET name=NEW.name, second_name=NEW.second_name, telephone=NEW.telephone, region=NEW.region,job=NEW.job
                WHERE id_client= OLD.id_client;
	UPDATE accounts
        SET number_acc = NEW.number_acc, balance = NEW.balance,costs = NEW.costs, profit = NEW.profit
                WHERE id_account= OLD.id_account;
       );
         
--DELETE 
CREATE OR REPLACE RULE client_balance_d AS ON DELETE TO client_balance_vw
        DO INSTEAD (
        DELETE FROM client 
                WHERE id_client = OLD.id_client;
        DELETE FROM accounts 
                WHERE id_account = OLD.id_account;
        );
-- � ������� CREATE TRIGGER ������� ������������� �����������

CREATE OR REPLACE FUNCTION clinet_balance() RETURNS TRIGGER
AS $clinet_balance$
DECLARE
   BEGIN
        IF TG_OP = 'INSERT' THEN
                INSERT INTO client (id_client,name,second_name, telephone,region,job)
                VALUES (NEW.name, NEW.second_name, NEW.telephone, NEW. region, NEW.job); 
		INSERT INTO accounts (number_acc, balance,costs, profit)
       		 VALUES (NEW.number_acc, NEW.balance,NEW.costs, NEW.profit);
                RETURN NEW;
        
        ELSIF TG_OP = 'UPDATE' THEN
                UPDATE Client
        		SET name=NEW.name, second_name=NEW.second_name, telephone=NEW.telephone, region=NEW.region,job=NEW.job
                	WHERE id_client= OLD.id_client;
		UPDATE accounts
        		SET number_acc = NEW.number_acc, balance = NEW.balance,costs = NEW.costs, profit = NEW.profit
                	WHERE id_account= OLD.id_account;
                RETURN NEW;
        
        ELSIF TG_OP = 'DELETE' THEN
                DELETE FROM client 
                WHERE id_client = OLD.id_client;
        DELETE FROM accounts 
                WHERE id_account = OLD.id_account;
        RETURN NULL;
      END IF;
      RETURN NEW;
    END;
$clinet_balance$ LANGUAGE plpgsql;

CREATE TRIGGER clinet_balance_vw
    INSTEAD OF INSERT OR UPDATE OR DELETE ON client_balance_vw 
    FOR EACH ROW EXECUTE PROCEDURE clinet_balance();
