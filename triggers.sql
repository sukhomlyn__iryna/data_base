-- 1.����������� ��������� ����������� (���������� ���� �� �.�. �2)

-- �������� ���������� ������� 
 
CREATE FUNCTION fmanager() RETURNS trigger 
AS $fmanager$
DECLARE
    BEGIN
        IF NEW.name IS NULL THEN
            RAISE EXCEPTION 'Input name of manager';
        END IF; 

        IF NEW.second_name IS NULL THEN
            RAISE EXCEPTION 'Input  second name of manager';
        END IF;

        IF NEW.number_client < 0 THEN
            RAISE EXCEPTION 'manager must have more clients than % ',NEW.number_client;
        END IF;

        IF NEW.job_hours < 4 THEN
            RAISE EXCEPTION 'manager must have more job hours per day than % ',NEW.job_hours;
        END IF;

        IF NEW.month_report < 2 THEN
            RAISE EXCEPTION 'mounth reports must be more than % ',NEW.month_report;
        END IF;

	IF NEW.year_report < 1 THEN
            RAISE EXCEPTION 'year reports must be more than %',NEW.year_report;
        END IF;
        RETURN NEW;
    END;
$fmanager$
LANGUAGE plpgsql;

-- �������� ��������

CREATE TRIGGER tmanager 
  BEFORE INSERT OR UPDATE ON manager
    FOR EACH ROW EXECUTE PROCEDURE fmanager();

--�������� ������ �������� 

INSERT INTO manager(name,second_name,number_client,job_hours,month_report,year_report) VALUES
('Anna','Kuharenko','2','3','1','1');
ERROR:  manager must have more job hours per day than 3
INSERT INTO manager(name,second_name,number_client,job_hours,month_report,year_report) VALUES
('Anna','Kuharenko','2','5','1','1');
ERROR:  mounth reports must be more than 1

--2.�����������, ������������� ��������� ������� ����� ��� ���������� ������ (�.�. �3 ��. 1.3 ��� 1.2)

-- �������� ���������� �������
       
CREATE  FUNCTION change_date() RETURNS trigger AS $$
        DECLARE
                n_date date;
        BEGIN
                SELECT c.current_dat FROM currancy c INTO n_date
                        WHERE c.id_currancy = NEW.id_currancy;
                
		UPDATE deal SET current_dat = n_date  
			WHERE id_currancy = NEW.id_currancy;       
                   
                RETURN NEW;     
    	END; 
$$ LANGUAGE  plpgsql;

-- �������� ��������

CREATE TRIGGER change_date
	AFTER UPDATE
	ON currancy FOR EACH ROW
        EXECUTE PROCEDURE change_date();

UPDATE deal_types SET current_date = '02.03.2037' WHERE id_type = 3;
--3. �������������� ���������� ��������� (�����������) ����(-��) (�������� INSERT/UPDATE/DELETE)
CREATE FUNCTION up_balance() RETURNS trigger AS $$
	DECLARE
		account_costs numeric;
		account_balanse numeric;
		account_profit	numeric;
        BEGIN
          	SELECT a.costs FROM accounts a INTO account_costs
                                WHERE a.id_account = NEW.id_account;
              
               	SELECT a.balance FROM accounts a INTO account_balanse
                                WHERE a.id_account = NEW.id_account; 
                SELECT a.profit FROM accounts a INTO account_profit
                                WHERE a.id_account = NEW.id_account;
                UPDATE accounts c SET c.balance = (account_balanse - account_costs + account_profit )
                        WHERE c.id_account = NEW.id_account;                          
                RETURN NEW;     
        END
$$ LANGUAGE plpgsql;
 
-- �������� ��������

CREATE TRIGGER up_balance_acc
    AFTER UPDATE ON accounts
        FOR EACH ROW EXECUTE PROCEDURE up_balance();

--4.�������������� ��������� ��� ����� �� ������

CREATE TABLE logs_table (

        message varchar(30);
        name_s varchar(30);
	price integer;
	number integer;
        time_add         TIMESTAMP WITHOUT TIME ZONE
);

CREATE FUNCTION Log_function() RETURNS TRIGGER AS $log_pledge$
	DECLARE
                mes varchar(30);
                name_s varchar(30);
	        price integer;
	        number integer;
		
        
        BEGIN
                IF    (TG_OP = 'INSERT') THEN
                        name_s = NEW.name;
			price = NEW.prace;
			number = NEW.number;
                        mes := 'New pledge';
                        INSERT INTO logs_table(mes ,name_s,price,number, time_add) 
                        VALUES (message,name_s,price,number, NOW());
                        RETURN NEW;

                ELSIF TG_OP = 'UPDATE' THEN
                        name_s = NEW.name;
			price = NEW.prace;
			number = NEW.number;
                        mes := 'Information of pledge updated';
                        INSERT INTO logs_table(mes ,name_s,price,number, time_add)
                        VALUES (message,name_s,price,number, NOW());
                        RETURN NEW;

                ELSIF TG_OP = 'DELETE' THEN
			name_s = OLD.name;
			price = OLD.prace;
			number = ILD.number;
                        mes := 'pledge was removed';
                        INSERT INTO logs_table(mes ,name_s,price,number, time_add)
                        VALUES (message,name_s,price,number, NOW());
                        RETURN OLD;
                END IF;
        END;
$log_pledge$ LANGUAGE plpgsql;

CREATE TRIGGER log_for_pledge_table
    AFTER INSERT OR UPDATE OR DELETE ON pledge
        FOR EACH ROW EXECUTE PROCEDURE Log_function();

