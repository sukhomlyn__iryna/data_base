CREATE ROLE database_creator WITH  SUPERUSER CREATEROLE CREATEDB LOGIN;
CREATE ROLE administrator WITH LOGIN;
CREATE ROLE usual_manager WITH LOGIN;
CREATE ROLE guest WITH NOLOGIN;

-- ���������� ��� ����. "pledge"
GRANT INSERT  ON pledge TO administrator,usual_manager;
GRANT SELECT  ON pledge TO administrator,usual_manager,guest;
GRANT UPDATE  ON pledge TO administrator;

REVOKE DELETE  ON pledge FROM administrator;

--���������� ��� ����. "manager"
GRANT INSERT  ON manager TO administrator,usual_manager;
GRANT SELECT  ON manager TO administrator,usual_manager,guest;
GRANT UPDATE  ON manager TO administrator;

--���������� ��� ����. "partner"
GRANT INSERT  ON partner TO administrator,usual_manager;
GRANT SELECT  ON partner TO administrator,usual_manager,guest;
GRANT UPDATE  ON partner TO administrator;

--���������� ��� ����. "currrancy"
GRANT SELECT  ON currancy TO administrator,usual_manager,guest;

--���������� ��� ����. "deal"
GRANT INSERT  ON deal TO administrator;
GRANT SELECT  ON deal TO administrator,usual_manager;
GRANT UPDATE  ON deal TO administrator,usual_manager;
REVOKE DELETE  ON deal FROM administrator;


--���������� ��� ����. "client"
GRANT INSERT  ON client TO administrator,usual_manager;
GRANT SELECT  ON client TO administrator,usual_manager,guest;
GRANT UPDATE  ON client TO administrator,usual_manager;


--���������� ��� ����. "accounts"
GRANT INSERT  ON accounts TO administrator,usual_manager;
GRANT SELECT  ON accounts TO administrator,usual_manager;
GRANT UPDATE  ON accounts TO administrator;